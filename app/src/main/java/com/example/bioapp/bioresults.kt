package com.example.bioapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView


class bioresults : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bioresults)
        // switch the intent to the new intent

        val bioIntent = intent
        val firstName = bioIntent.getStringExtra("firstName")
        val lastName = bioIntent.getStringExtra("lastName")
        val interests = bioIntent.getStringExtra("interests")
        val gradYear = bioIntent.getStringExtra("gradYear")
        val school =  bioIntent.getStringExtra("school")
        val degree =  bioIntent.getStringExtra("degree")
        val major =  bioIntent.getStringExtra("major")
        //retreive the values from the last intent to new variables

        val resultTextView = findViewById<TextView>(R.id.bioTextView)
        resultTextView.text =  firstName + " " + lastName + " Graduated in " + gradYear + " from " +
                school + " with a " + degree + " in " +
                major + " their favorite activities include " + interests;
        //update the text view with the new values
    }
}
