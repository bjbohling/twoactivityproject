package com.example.bioapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var degreeS = "College degree"
    //default variable if no radio button for degree is selected


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btnNewActivity = findViewById(R.id.btnSubmit) as Button
        //button for submit that is at the bottom of the form

        val firstNameE = findViewById<EditText>(R.id.firstNameEdit) as EditText
        val lastNameE = findViewById<EditText>(R.id.lastNameEdit) as EditText
        val interestsE = findViewById<EditText>(R.id.interestsEdit) as EditText
        val schoolE = findViewById<EditText>(R.id.schoolEdit) as EditText
        val gradYearE = findViewById<EditText>(R.id.gradEdit) as EditText
        //Objects to hold values of the edittext fields

        val majorE = findViewById<EditText>(R.id.spinner) as Spinner
        //object to hold value of the spinner for college majors

        val spinner: Spinner = majorE
        ArrayAdapter.createFromResource(
            this,
            R.array.majors_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        //Code to show the spinner the list of Strings to take values from. The strings are held in
        //the values folder under strings.xml. Code adapted from the official Kotlin android dev
        //documentation https://developer.android.com/guide/topics/ui/controls/spinner



        btnNewActivity.setOnClickListener {
            //this function will run when the "Submit" button is clicked

            val firstNameS = firstNameE.text.toString()
            val lastNameS = lastNameE.text.toString()
            val interestsS = interestsE.text.toString()
            val schoolS = schoolE.text.toString()
            val gradYearS = gradYearE.text.toString()
            //These variables will take the Edit text boxes from earlier
            // and take the string value of those objects to be stored in
            //these variables

            val majorS = majorE.selectedItem.toString()
            //variable for the major from the spinner

            val intent = Intent(this, bioresults::class.java)
            //this will switch the intent to the bioresults activity where
            //the results of the generated bio will be displayed

            intent.putExtra("firstName",firstNameS )
            intent.putExtra("lastName",lastNameS )
            intent.putExtra("interests",interestsS )
            intent.putExtra("gradYear",gradYearS )
            intent.putExtra("school",schoolS )
            //pass the values of the input to the next intent

            intent.putExtra("major",majorS )
            intent.putExtra("degree",degreeS )
            //pass the values of the input to the next intent

            startActivity(intent);
            //switch intents to the results page
        }


        radioGroup1.setOnCheckedChangeListener(
            //change listener for when the value of the radio button is changed
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
                degreeS = radio.text.toString()
                //when the radio button is changed, the value of degreeS, which we pass to the
                //next intent, is updated as well
            })
        }


}



